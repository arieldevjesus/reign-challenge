# Reign-Challenge



## To start setting up the project  

Step 1:  run: ```npm install```


Step 2:  rename the file ```.env.example```  to  ```.env```


Step 3:  In the ```.env``` file, set the ```PORT```   (e.g.  PORT=5000)

Step 4:  set the Mongoose connection ```MONGODB_CNN```  
(e.g. MONGODB_CNN= mongodb://username:password@host:port/database?options...)
 

## To run the App

Run : ```npm run start```

## To run the Tests

Run : ```npm run test```