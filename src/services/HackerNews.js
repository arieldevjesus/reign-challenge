const { default: axios } = require("axios");
const hitSchema = require('../api/models/hit');

//Inserts the news into de database (Only those that are not already there)
const saveNews = async(data = []) => {
    let newHitsCounter = 0; //to track how many hits have been inserted
    for (let i = 0; i < data.length; i++) {
        const hit = data[i];
        const exists = await hitSchema.exists({ objectID: hit.objectID });
        if (!exists) {
            await hitSchema.create(data[i]);
            newHitsCounter++;
        }
    }
    return newHitsCounter;
}

//Gets the news from the api 
const getNews = async() => {
    try {
        const response = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs');
        const newHitsCounter = await saveNews(response.data.hits);
        console.log(`Db updated || Total news inserted: ${newHitsCounter}  || Next update: within 1 hour`);
    } catch (err) {
        console.log("Unable to update the db");
        console.log(err);
    }
}

module.exports = getNews;