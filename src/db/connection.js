const mongoose = require('mongoose');
require('dotenv').config();
const connectDb = async() => {
    try {
        await mongoose.connect(process.env.MONGODB_CNN);
        console.log("Db connected successfully");
    } catch (err) {
        console.log("There's a problem with the db");
        console.log(err);
    }
}
const closeConnection = async() => {
    try {
        await mongoose.connection.close();
    } catch (err) {
        console.log(err);
    }
}
module.exports = { connectDb, closeConnection };