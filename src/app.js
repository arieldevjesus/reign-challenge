const express = require('express');

const newsRouter = require('./api/routes/news');
const app = express();


app.use(express.json()); //To handle json requests

//Setting the routes
app.use('/api/news', newsRouter);
app.use('*', (req, res) => { res.status(404).json({ msg: 'Oops,Not found.' }) });

module.exports = app;