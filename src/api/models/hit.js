const { Schema, model } = require('mongoose');
const highlightResultSchema = require('./highlightResult');
const mongoosePaginate = require('mongoose-paginate-v2');

const hitSchema = new Schema({
    created_at: Date,
    title: String,
    url: String,
    author: String,
    points: Number,
    story_text: String,
    comment_text: String,
    num_comments: Number,
    story_id: Number,
    story_title: String,
    story_url: String,
    parent_id: Number,
    created_at_i: Number,
    _tags: [String],
    objectID: Number,
    _highlightResult: highlightResultSchema
});

hitSchema.plugin(mongoosePaginate); // To be able to paginate the results
module.exports = model('hit', hitSchema);