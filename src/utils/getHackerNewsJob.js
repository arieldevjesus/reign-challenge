const schedule = require('node-schedule');
const getNews = require('../services/HackerNews');
const getNewsJob = () => {
    schedule.scheduleJob('0 * * * *', () => {
        getNews().then();
    });
}
module.exports = getNewsJob;