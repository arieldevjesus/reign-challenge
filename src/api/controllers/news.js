const hitSchema = require('../models/hit');
const paginateOptions = require('../../utils/paginateOptions');

const getNews = async(req, res) => {
    const { page, title, tag: tags, author } = req.query;
    const options = paginateOptions(5, page, 'hits'); //To paginate the results

    const filterQuery = { //To filter. Only the params sent by the user are added to the filterQuery Object
        ...(author && { author: author }),
        ...(title && { author: title }),
        ...(tags && { _tags: { $all: tags } })
    }

    try {
        const news = await hitSchema.paginate(filterQuery, options);
        res.status(200).json(news);
    } catch (err) {
        res.status(500).json({ msg: 'Oops,Something went wrong' });
        console.log(err);
    }
}


const removeOne = async(req, res) => {
    const { id } = req.params;
    if (!parseInt(id)) {
        return res.status(400).json({ msg: "Invalid id" });
    }
    try {
        const hitToBeRemoved = await hitSchema.findOne({ objectID: id });
        if (!hitToBeRemoved) {
            return res.status(404).json({ msg: "The resource you are trying to remove does not exist." });
        }
        await hitToBeRemoved.remove();
        res.status(200).json({ msg: 'Resource removed!' });
    } catch (err) {
        res.status(500).json({ msg: 'Oops,Something went wrong' });
        console.log(err)
    }
}


module.exports = { getNews, remove: removeOne };