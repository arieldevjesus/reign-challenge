//Entry point
require('dotenv').config();
const { connectDb } = require('./db/connection');
const getNewsJob = require('./utils/getHackerNewsJob');
const getNews = require('./services/HackerNews');

const app = require('./app');
const port = process.env.PORT || 3000;

connectDb().then(() => { //Connects the db
    getNews(); //Populates the db for the first time, when the server starts
    getNewsJob(); //Gets the data from the Hacker News Api at hourly intervals.
});

app.listen(port, () => {
    console.log("Server running on port: " + port);
});