const request = require('supertest');
const { connectDb, closeConnection } = require('../src/db/connection');
const app = require('../src/app');

describe("DELETE new endpoint /api/news", () => {

    test("It should respond with a json as a content type", async() => {
        await connectDb();
        const response = await request(app).delete("/api/news/29875334").send();
        expect(response.headers["content-type"]).toEqual(
            expect.stringContaining("json")
        );
    });

    test("It should respond with a 400 status when an invalid id is sent 'string': Mango ", async() => {
        const response = await request(app).delete("/api/news/mango").send();
        expect(response.statusCode).toBe(400);
    });

    test("It should respond with a 404 status code,when the new they're trying to delete, does not exist :123", async() => {
        const response = await request(app).delete("/api/news/123").send();
        expect(response.statusCode).toBe(404);
    });

});

describe("GET news endpoint /api/news", () => {
    test("It should respond with a json as a content type", async() => {
        const response = await request(app).get("/api/news/29875334").send();
        await closeConnection();
        expect(response.headers["content-type"]).toEqual(
            expect.stringContaining("json")
        );
    });
});