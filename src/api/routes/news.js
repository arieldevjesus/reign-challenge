const newsController = require('../controllers/news');
const { Router } = require('express');
const router = Router();

router.get('/', newsController.getNews);
router.delete('/:id', newsController.remove);

module.exports = router;