const paginateOptions = (limit, page, docsName) => {
    const options = {
        limit: limit,
        page: parseInt(page) || 1, //if the page query param sent by the user is not valid,it will be 1
        customLabels: { docs: docsName, totalDocs: 'total' + docsName }, //renaming some of the default paginate labels'
        select: '-__v -_id' //excludes the fields __v & -_id
    }
    return options;
}

module.exports = paginateOptions;