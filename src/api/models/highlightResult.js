const { Schema } = require('mongoose');

//Defining the necessary schemas

const baseSchema = new Schema({ //Schema for author,story_title & story_url. (They have the same properties)
    value: { type: String },
    matchLevel: { type: String },
    matchedWords: [String]
});

const commentTextSchema = new Schema({
    value: String,
    matchLevel: String,
    fullyHighlighted: Boolean,
    matchedWords: [String]
});

const highlightResultSchema = new Schema({
    author: baseSchema,
    comment_text: commentTextSchema,
    story_title: baseSchema,
    story_url: baseSchema
});

module.exports = highlightResultSchema;